import { combineReducers } from 'redux';
import reducerPost from './reducer_post';
import { reducer as formReducer } from 'redux-form';


const Reducers = combineReducers({
  // state: (state = {}) => state,
   reducerPost: reducerPost 
});

export default Reducers;