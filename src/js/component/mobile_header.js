var React=require('react');
var ReactDOM=require('react-dom');
import { Row, Col } from 'antd';
import {Menu,Icon,Modal,Form,Tabs,Input,Checkbox,Button} from 'antd';

const SubMenu=Menu.SubMenu;
const MenuItemGroup=Menu.ItemGroup;
const TabPane=Tabs.TabPane;
const FormItem=Form.Item;

import { WrapTabPanel } from './innerComponents/header-modal';


class SubcompoMenu extends React.Component{
	constructor(props){
		super(props);
		this.handleChange=this.handleChange.bind(this);
		this.showModal=this.showModal.bind(this);
		this.logout=this.logout.bind(this);
	}

	handleChange(e){
		console.log(e.className);
	}

	showModal(){
		this.props.onModalChange(true);
	}

	logout(){
		this.props.login(false);
		localStorage.setItem('username','');
	}

	render(){
		console.log("user login");
		console.log(this.props.userLogin);
		const usershow = this.props.userLogin? <div className="longoutWrap ant-col-8 ant-col-pull-16"><div className="ant-col-8"><Icon type="smile-o" /></div> <div className="ant-col-8" onClick={this.logout}><Icon type="logout" /></div></div>: <div className="ant-col-8 ant-col-pull-16" onClick={this.showModal}><Icon type="login" /></div>
		return(
				<div className="menuWrap">
					<Menu mode="inline" onTouchStart={this.handleChange} className="ant-col-16 ant-col-push-8">
						<SubMenu key="menu" title={<span><Icon type="setting" /><a href="#">Menu</a></span>}>
							<Menu.Item key="headline"><a href="#">Headline</a></Menu.Item>
							<Menu.Item key="society"><a href="#">Society</a></Menu.Item>
							<Menu.Item key="internal"><a href="#">Internal</a></Menu.Item>
							<Menu.Item key="international"><a href="#">Iternational</a></Menu.Item>
							<Menu.Item key="entertainment"><a href="#">Entertainment</a></Menu.Item>
							<Menu.Item key="sport"><a href="#">Sport</a></Menu.Item>
							<Menu.Item key="technology"><a href="#">Technology</a></Menu.Item>
							<Menu.Item key="fashion"><a href="#">Fashion</a></Menu.Item>				
						</SubMenu>
					</Menu>
					{usershow}
				</div>
			)
	}
}


export default class MobileHeader extends React.Component{
	constructor(props){
		super(props);
		this.state={
			current: 'headline',
			login: false,
			modalVisible: true,
			topValue: 300,
			leftValue: 0
		};
			this.handleClick=this.handleClick.bind(this);
			this.hideModal=this.hideModal.bind(this);
			this.showModal=this.showModal.bind(this);
			this.unVisible=this.unVisible.bind(this);
			this.handleDragStart=this.handleDragStart.bind(this);
			this.handleDrag=this.handleDrag.bind(this);
			this.handleDragEnd=this.handleDragEnd.bind(this);
			this.onModalChange=this.onModalChange.bind(this);
			this.haslogin=this.haslogin.bind(this);
  		};
	componentWillMount(){
				// check whether user loged in
		if(localStorage.getItem('username')){   
			this.setState({
				login: true,
				modalVisible: false
			});
		}

	}
	haslogin(val){
		this.setState({
			login: val
		})
	}


	handleClick(e){
		return(
				this.setState({
					current: e.key
				})
			)
	}

	 showModal(){
	 	return(
	 			this.setState({
	 				modalVisible: true
	 			})
	 		)
	 }

	 hideModal(){
	 	return(
	 			this.setState({
	 				modalVisible: false
	 			})
	 		)
	 }

	 unVisible(result){
	 	return(
	 			this.setState({
	 				modalVisible: result
	 			})
	 		)
	 }
	 handleDragStart(e){
	 	let ele=document.getElementById("modalTitle");
	 	let viewportOffset=ele.getBoundingClientRect();
	 	cltX=e.clientX;
	 	cltY=e.clientY;
	 	startLeft=viewportOffset.left;
	 	startTop=viewportOffset.top;
	 }
	 handleDrag(e){
	 }
	 handleDragEnd(e){
	 	
	 	cltX=e.clientX-cltX;
	 	cltY=e.clientY-cltY;
	 	let endLeft=startLeft+cltX;
	 	let endTop=startTop+cltY;
	  	return(
	  		this.setState({
	  			topValue: endTop,
	  			leftValue: endLeft
	  		})
	  		)
	  }

	  onModalChange(modalVisible){
	  	this.setState({modalVisible})	
	  }


	render(){
		let topValue=this.state.topValue;
		let aply=((this.state.leftValue)==0)?true:false;
		const styleWrap={
			top: topValue>0?topValue:0,
			left: aply?"auto":(leftValue>0?leftValue:0),
			margin: aply?"auto":0
		}

		return(
			<div id="mobile_header">
				<div className="menu">
					<Row>
					  <Col span={6}><a className="logo" href="#"><img src="../../img/logo.jpg" /></a></Col>
					  <Col span={6}></Col>
				      <Col span={12}>
						<SubcompoMenu userLogin={this.state.login} login={this.haslogin} onModalChange={this.onModalChange} />
				      </Col>
				    </Row>
			    </div>
				<Modal draggable="true" style={styleWrap} title="Modal" visible={this.state.modalVisible} onCancel={this.hideModal} className="pc_login">
					<span id="modalTitle" className="modal-title" draggable="true" onDragStart={this.handleDragStart} onDrag={this.handleDrag} onDragEnd={this.handleDragEnd}>    </span>
					<WrapTabPanel haslogin={this.haslogin} unVisible={this.unVisible} />
				</Modal>
			</div>	

			)
	}
}
