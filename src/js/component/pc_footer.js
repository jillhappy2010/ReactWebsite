var React=require('react');
var ReactDOM=require('react-dom');
import {Row,Col} from 'antd';

export default class PcFooter extends React.Component{
	render(){
		return(
			<div id="pc_footer">
				<Row>
					<Col span={24}>
						&copy; 2017 ReactNews all right reserved 
					</Col>
				</Row>
			</div>	

			)
	}
}
