var React=require('react');
var ReactDOM=require('react-dom');
import { Row, Col } from 'antd';
import { Form,Input,Checkbox,Modal,Button,Tabs } from 'antd';
import {Menu,Icon} from 'antd';
const SubMenu=Menu.SubMenu;
const MenuItemGroup=Menu.ItemGroup;
const TabPane=Tabs.TabPane;
const FormItem=Form.Item;

//here is the copy code segment 

import { WrapTabPanel } from './innerComponents/header-modal';
var cltX=0;
var cltY=0;
var startLeft=0;
var startTop=0;

export default class PcHeader extends React.Component{
	constructor(props){
		super(props);
		this.state={
			current: 'headline',
			login: false,
			modalVisible: true,
			topValue: 300,
			leftValue: 0
		};
		this.handleClick=this.handleClick.bind(this);
		this.hideModal=this.hideModal.bind(this);
		this.showModal=this.showModal.bind(this);
		this.unVisible=this.unVisible.bind(this);
		this.handleDragStart=this.handleDragStart.bind(this);
		this.handleDrag=this.handleDrag.bind(this);
		this.handleDragEnd=this.handleDragEnd.bind(this);
		this.haslogin=this.haslogin.bind(this);
		this.logout=this.logout.bind(this);
	}

	componentWillMount(){
				// check whether user loged in
		if(localStorage.getItem('username')){   
			this.setState({
				login: true,
				modalVisible: false
			});
		}

	}
	componentDidMount(){
		var ele=document.getElementById('pc_header').getElementsByTagName('li')[0];
		ele.className+=" ant-menu-item-selected";
		ele.setAttribute('aria-selected',true);
	}

	componentWillUpdate() {
		var ele=document.getElementById('pc_header').getElementsByTagName('li')[0];
	  	ele.setAttribute('class','ant-menu-item');
	  	ele.setAttribute('aria-selected',false);
	}
	componentDidUpdate(){
		console.log("check the login status");
		console.log(this.state.login);
	}
	logout(){
		this.setState({
			login: false
		})
		localStorage.setItem('username','');
	}
	haslogin(val){
		this.setState({
			login: val
		})
	}

	handleClick(e){
		return(
				this.setState({
					current: e.key
				})
			)
	}

	 showModal(){
	 	return(
	 			this.setState({
	 				modalVisible: true
	 			})
	 		)
	 }

	 hideModal(){
	 	return(
	 			this.setState({
	 				modalVisible: false
	 			})
	 		)
	 }

	 unVisible(result){
	 	return(
	 			this.setState({
	 				modalVisible: result
	 			})
	 		)
	 }
	 handleDragStart(e){
	 	let ele=document.getElementById("modalTitle");
	 	let viewportOffset=ele.getBoundingClientRect();
	 	cltX=e.clientX;
	 	cltY=e.clientY;
	 	startLeft=viewportOffset.left;
	 	startTop=viewportOffset.top;
	 }
	 handleDrag(e){
	 }
	 handleDragEnd(e){
	 	
	 	cltX=e.clientX-cltX;
	 	cltY=e.clientY-cltY;
	 	let endLeft=startLeft+cltX;
	 	let endTop=startTop+cltY;
	  	return(
	  		this.setState({
	  			topValue: endTop,
	  			leftValue: endLeft
	  		})
	  		)
	 }

	render(){
		const userInfor_logout=<div className="logoutWrap"><Button className="userInfor" type="primary" ghost>User Informatio</Button><Button type="default" onClick={this.logout} ghost>Logout</Button></div>;
		let topValue=this.state.topValue;
		let leftValue=this.state.leftValue;
		let aply=((this.state.leftValue)==0)?true:false;
		const styleWrap={
			top: topValue>0?topValue:0,
			left: aply?"auto":(leftValue>0?leftValue:0),
			margin: aply?"auto":0
		}
		// login logout user information button
		const menuBtn=this.state.login?userInfor_logout:<Button type="primary" onClick={this.showModal}>register/login</Button>;
			return(
			<div id="pc_header">
				<Row>
					<Col span={4}><a className="logo" href="#"><img src="../../img/logo.jpg" alt="logo" /></a></Col>
				    <Col span={20} className="menuBar">
					    <Row>
						    <Col span={20}>
							    <Menu mode="horizontal"  onClick={this.handleClick} >
								    <Menu.Item key="headline" id="headline">
								    	<Icon type="mail" />头条
								    </Menu.Item>
								     <Menu.Item key="society">
								    	<Icon type="mail" />社会
								    </Menu.Item>
								     <Menu.Item key="internal">
								    	<Icon type="mail" />国内
								    </Menu.Item>
								     <Menu.Item key="international">
								    	<Icon type="mail" />国际
								    </Menu.Item>
								     <Menu.Item key="entertainment">
								    	<Icon type="mail" />娱乐
								    </Menu.Item>
								     <Menu.Item key="sports">
								    	<Icon type="mail" />体育
								    </Menu.Item>
								     <Menu.Item key="techology">
								    	<Icon type="mail" />科技
								    </Menu.Item>
								     <Menu.Item key="fashion">
								    	<Icon type="mail" />时尚
								    </Menu.Item>
							    </Menu>
						    </Col>
						    <Col span={4}>
							    <div className="menuBtn">
							    	{menuBtn}
							    </div>
						    </Col>
					    </Row>
				    </Col>
			    </Row>
				<Modal draggable="true" style={styleWrap} title="Modal" visible={this.state.modalVisible} onCancel={this.hideModal} className="pc_login">
					<span id="modalTitle" className="modal-title" draggable="true" onDragStart={this.handleDragStart} onDrag={this.handleDrag} onDragEnd={this.handleDragEnd}>    </span>
					<WrapTabPanel haslogin={this.haslogin} unVisible={this.unVisible} />
				</Modal>
			</div>	

			)
	}
}
