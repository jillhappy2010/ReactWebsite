var React=require('react');
var ReactDOM=require('react-dom');
import {Layout} from 'antd';
const {Header,Footer,Sider,Content}=Layout;
import MobileHeader from './mobile_header';
import MobileFooter from './mobile_footer';

export default class MobileIndex extends React.Component{
	render(){
		return(
			<div id="mobile">
				<Layout>
					<Header>
						<MobileHeader />
					</Header>
					<Content>Content</Content>
					<Footer>
						<MobileFooter />
					</Footer>
				</Layout>
			</div>	

			)
	}
}
