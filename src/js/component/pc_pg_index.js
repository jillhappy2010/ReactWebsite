var React=require('react');
var ReactDOM=require('react-dom');
import {Layout,Row,Col} from 'antd';
const {Header,Footer,Sider,Content}=Layout;
import PcHeader from './pc_header';
import PcFooter from './pc_footer';
import CstCarousel from './innerComponents/carousel';
import CstNewsList from './innerComponents/newslist';
import CstSidebar from './innerComponents/sidebar';

export default class PcIndex extends React.Component{
	render(){
		return(
			<div id="pc">
				<Layout>
					<Header>
						<PcHeader />
					</Header>
					<Content>
						<Row>
							<Col span={8} className="cstNewsblock">
							  <CstCarousel />
							  <CstNewsList cstLayout='block' count={3} cstCategory='top' cstCategoryTitle='news category' sglCol={8} />
							  <CstNewsList cstLayout='block' count={6} cstCategory='top' cstCategoryTitle='news category' sglCol={8} />
							</Col>
							<Col className="cstNewsList" span={12}>
							  <CstNewsList cstLayout='list' count={10} cstCategory='top' cstCategoryTitle='news category' sglCol={24} />
							</Col>
							<Col span={4}>
								<CstSidebar />
							</Col>
						</Row>
					</Content>
					<Footer>
						<PcFooter />
					</Footer>
				</Layout>
			</div>	

			)
	}
}
