
import React from 'react';
import ReactDOM from 'react-dom';
import { Row, Col } from 'antd';
import { Form,Input,Checkbox,Modal,Button,Tabs } from 'antd';
import {Menu,Icon} from 'antd';
const SubMenu=Menu.SubMenu;
const MenuItemGroup=Menu.ItemGroup;
const TabPane=Tabs.TabPane;
const FormItem=Form.Item;


class RegisterForm extends React.Component{
	constructor(props){
		super(props);
		this.state={
			confirmDirty: false,
			userNickName: '',
			insertMessage: 'none',
			valid: false
		}
		this.handleRegister=this.handleRegister.bind(this);
		this.handleConfirmBlur=this.handleConfirmBlur.bind(this);
		this.checkPassword=this.checkPassword.bind(this);
		this.checkConfirm=this.checkConfirm.bind(this);
		this.handleChange=this.handleChange.bind(this);
	}
	componentDidMount(){
		this.props.form.validateFields();
	}

	handleConfirmBlur(e){
		const value=e.target.value;
		this.setState=({
			confirmDirty: this.state.confirmDirty||!!value});
		
	}

	handleChange(fieldsError){
		return Object.keys(fieldsError).some(field=>fieldsError[field]);
	}

	checkPassword(rule,value,callback){
		const form=this.props.form;
		if(value&&value!==form.getFieldValue('passWord')){
			callback('Two passwords that you enter is inconsistent!');
		}
		else{
			callback();
		}
	}  

	checkConfirm(rule,value,callback){
		const form=this.props.form;
		if(value&&this.state.confirmDirty){
			form.validateFields(['confirm_passWord',{force:true}]);
		}
		callback();
	} 

	 handleRegister(e){
		 	e.preventDefault();
		 	this.props.unVisible(false);
		 	this.props.form.validateFieldsAndScroll((err,values)=>{
		 		if(!err){
		 			console.log('Received values of form', values);
		 		}
		 	});   
		 	var myFetchOptions={
		 		method: 'GET'
		 	};
		 	var formData=this.props.form.getFieldsValue();
		 	console.log(formData);
		 	fetch("http://newsapi.gugujiankong.com/Handler.ashx?action=register&username="+formData.users+"&password="+formData.passWord+"&r_confirmPassWord="+formData.confirm_passWord,myFetchOptions).
		 		then(response=>response.json()).then(json=>{
		 			console.log("the formData is xxxxx");
		 			console.log(formData);
		 		  this.setState({
		 		  	userNickName: formData.users,
		 		  	insertMessage: 'inline'
		 		  });
		 		});
		 }
	
	render(){
		const {getFieldDecorator,getFieldsError,getFieldError,isFieldTouched}=this.props.form;

		return(
				<Form onChange={this.handleChange} onSubmit={this.handleRegister}>
								<FormItem>
									{getFieldDecorator('users',{
										rules:[{type: 'email',message:'Please input your email as username'},{
											required: true, message: 'plese input your Email',
										}],
									})(
										<Input prefix={<Icon type="user" style={{fontSize:13}} />} placeholder="username" />
									)}
								</FormItem>
								<FormItem>
									{getFieldDecorator('passWord',{
										rules: [{required:true,message: 'please set up your password'},{
											validator: this.checkConfirm,
										}],
									})(
										<Input prefix={<Icon type="user" style={{fontSize:13}} />} type="password" placeholder="set password" />
									)}
								</FormItem>
								<FormItem>
									{getFieldDecorator('confirm_passWord',{
										rules: [{required:true,message: 'please confirm your password'},{
											validator: this.checkPassword,
										}],
									})(
										<Input prefix={<Icon type="user" style={{fontSize:13}} />}  type="password" placeholder="confirm password" />
									)}
								</FormItem>
								<FormItem>
									<Button disabled={this.handleChange(getFieldsError())} type="primary" htmlType="submit" className="register-form-button">Register</Button>
									<span style={{marginLeft: 10+'px',display: this.state.insertMessage}}>You've already registered, please Login </span>
								</FormItem>
							</Form>
			)
	}
}
class LoginForm extends React.Component{
	constructor(props){
		super(props);
		this.handleLogin=this.handleLogin.bind(this);
	}
	 handleLogin(e){
	 	e.preventDefault();
	 	this.props.unVisible(false);
	 	let formval=this.props.form.getFieldsValue();
	 	fetch('http://newsapi.gugujiankong.com/Handler.ashx?action=login&username='+formval.userName+'&password='+formval.passWord)
	 		.then(response => response.json())
	 		.then(data => {
	 			console.log("the data is xxxxx");
	 			console.log(data);
	 			if(data){
		 			if(data.UserId){
		 				console.log("change haslogin");
		 				console.log(data);
		 				this.props.haslogin(true);
		 				if(typeof(Storage) !== 'undefined'){
		 					localStorage.setItem('username',data.NickUserName);
		 				}else{
		 					console.log("sorry browser does not support localstorage");
		 				}
		 			}; 
		 		}else{
		 			alert("login failed");
		 		};
	 		})
	 		.catch(error => {
	 			console.log(error);
	 		})
	 }
	 render(){
		const {getFieldDecorator}=this.props.form;
		return(
				<Form onSubmit={this.handleLogin} className="login-form">
								<FormItem>
									{getFieldDecorator('userName',{
										rules:[{required: true, message: 'Please input your username!'}],
									})(
										<Input prefix={<Icon type="user" style={{fontSize:13}}/>} placeholder="username" />
									)}
								</FormItem>
								<FormItem>
									{getFieldDecorator('passWord',{
										rules:[{required: true, message: 'Please input your passWord!'}],
									})(
										<Input  type="password" prefix={<Icon type="user" style={{fontSize:13}} />} placeholder="passWord" />
									)}
								</FormItem>
								<FormItem>
									{getFieldDecorator('remember',{
										valuePropName: 'checked',
										initialValue: true
									})(
										<Checkbox>Remember me</Checkbox>
									)}
									<a className="login-form-forgot" href="">Forgot passWord</a>

								</FormItem>
								<FormItem>
									<Button type="primary" htmlType="submit" className="login-form-button">Log in</Button>
								</FormItem>
					</Form>
			)
	}
}

const WrapLoginForm=Form.create()(LoginForm);
const WrapRegisterForm=Form.create()(RegisterForm);


class TabPanel extends React.Component{
	constructor(props){
		super(props);
		this.state={
			haslogin: false
		};
		this.haslogin=this.haslogin.bind(this);
	}

	componentDidUpdate(){
		console.log("here is the tabpanel did mount, check login states");
		console.log(this.state.haslogin);
	}

	haslogin(val){
		this.props.haslogin(val);
		this.setState({
			haslogin: val
		})
	}	

	render(){
		return(
				<Tabs defaultActiveKey="1">
						<TabPane tab={<span><Icon type="apple" />Login</span>} key="1">
							<WrapLoginForm haslogin={this.haslogin} unVisible={this.props.unVisible} />
						</TabPane>
						<TabPane tab={<span><Icon type="android" />Register</span>} key="2">
							用户中心
							<WrapRegisterForm  unVisible={this.props.unVisible} />
						</TabPane>
					</Tabs>
			)
	}
}

export const WrapTabPanel=Form.create()(TabPanel);