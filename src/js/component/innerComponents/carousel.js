import React from 'react';
import { Component } from 'react';
import { Carousel } from 'antd';

export default class CstCarousel extends Component{
	render(){
		return (
				<div className="carouselWrap">
				  <Carousel autoplay>
				    <div>
				    	<img src="http://www.dolcevitatravelmagazine.com/wp-content/uploads/2014/10/Cook-Islands-Background.jpg" />
				    </div>
				    <div>
				    	<img src="http://www.dolcevitatravelmagazine.com/wp-content/uploads/2014/10/Cook-Islands-Background.jpg" />
					 </div>
				    <div>
				    	<img src="http://www.dolcevitatravelmagazine.com/wp-content/uploads/2014/10/Cook-Islands-Background.jpg" />
				    </div>
				    <div>
				    	<img src="http://www.dolcevitatravelmagazine.com/wp-content/uploads/2014/10/Cook-Islands-Background.jpg" />
				    </div>
				  </Carousel>
				</div>
			)
	}
}