import React from 'react';
import { Component } from 'react';
import { Card,Tab,Row,Col } from 'antd';
import {Link} from 'react-router-dom';

let newsWrap=<div>loading....</div>;

/*****  customize: cstCategory: define content category, 
         count: the number of news in the list, 
         layout: bock / list 
		sglCol: number of single column 
   ****************************/ 


export default class NewsList extends Component{
	constructor(props){
		super(props);
		this.state={
			news: '',
			 key: 'tab1'
		};
		this.onTabChange=this.onTabChange.bind(this);
	}

	componentWillMount(){
		const myFetchOption={
			method: 'get'
		};
		fetch('http://newsapi.gugujiankong.com/Handler.ashx?action=getnews&type='+this.props.cstCategory+'&count='+this.props.count)
			.then(response => response.json())
			.then(data => {
				this.setState({
					news: data
				});
			});
	}

	onTabChange(key,type){
		this.setState({
			[type]: key
		})
	}
	 
	render(){
		if(this.state.news.length>1){
			if(this.props.cstLayout === 'list')
				newsWrap = this.state.news.map((news,index)=>(<li key={index}><p><Link to={`/details/${news.uniquekey}`}>{news.title}</Link></p></li>))
			if(this.props.cstLayout === 'block')
				newsWrap = this.state.news.map((news,index)=>(
					<li key={index} className={"ant-col-"+this.props.sglCol}>
							<Link to={`/details/${news.Id.Pid}`}>
								<img src={news.thumbnail_pic_s} />
								<p>{news.title}</p>
							</Link>
					</li>
				))
		}
		return (
	            <Card title={this.props.cstCategoryTitle}>
			          <ul>{newsWrap}</ul>
		        </Card>
			)
	}
}