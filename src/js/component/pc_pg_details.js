import React from 'react';
import {Component} from 'react';
import PcHeader from './pc_header';
import {Layout,Row,Col} from 'antd';
const {Header,Footer,Sider,Content}=Layout;
import NewsDetails from './innerComponents/newsdetails';


export default class PcNewsDetails extends Component{
	render(){
		return(
				<div id="pgDetails">
					<Layout>
						<Header>
							<PcHeader />
						</Header>
						<Content>
							<NewsDetails urlParam={this.props.match.params.id} />
						</Content>
						<Footer>
						</Footer>
					</Layout>
				</div>
			)
	}
}