var React=require('react');
var ReactDOM=require('react-dom');
import '../../node_modules/antd/dist/antd.css';
import PcIndex from './component/pc_pg_index';
import MobileIndex from './component/mobile_pg_index';
import PcNewsDetails from './component/pc_pg_details';
import { BrowserRouter as Router,Route, Switch } from 'react-router-dom';
import MediaQuery from 'react-responsive';


export default class App extends React.Component{
	render(){
		return(
			<div>
				<MediaQuery query='(min-device-width: 1224px)'>
					<Router>
						<div>
							<Switch>
								<Route path="/details/:id" component={PcNewsDetails} />
								<Route path="/details" component={PcNewsDetails} />
								<Route path="/" component={PcIndex} />
							</Switch>
						</div>
					</Router>
				</MediaQuery>
				<MediaQuery query='(max-device-width: 1224px'>
					<MobileIndex />
				</MediaQuery>
			</div>

			)
	}
}
